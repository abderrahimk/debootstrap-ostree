#!/bin/bash
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>

#####################################################
#          Usage and command line parsing           #
#####################################################
function usage () {
    echo "Usage: "
    echo "  debootstrap-ostree.sh [OPTIONS]"
    echo
    echo "Runs debootstrap and commits the result to an ostree repository"
    echo
    echo "Options:"
    echo
    echo "  -h --help                    Display this help message and exit"
    echo "  -c --config   <filename>     The multistrap configuration file to use"
    echo "  -a --arch     <arch>         The debian architecture name, e.g. amd64 or i386"
    echo "  -w --workdir  <directory>    A directory to do the work, default is to use a tempdir"
    echo "  -o --ostree   <directory>    A directory of a local ostree repository to commit to"
    echo "  -g --gpg      <gpg>          A GPG key to sign the ostree commits with"
    echo "  --gpg-home    <directory>    A GPG home directory, e.g. /home/user/.gnupg"
    echo "  "
    exit 1;
}

arg_arch=
arg_workdir=
arg_ostree=
arg_config=
arg_gpg=""
arg_gpg_home=""

while : ; do
    case "$1" in 
	-h|--help)
	    usage;
	    shift ;;

	-a|--arch)
	    arg_arch=${2};
	    shift 2 ;;

	-w|--workdir)
	    arg_workdir=${2};
	    shift 2 ;;

	-o|--ostree)
	    arg_ostree=${2};
	    shift 2 ;;

	-c|--config)
	    arg_config=${2};
	    shift 2 ;;

	-g|--gpg)
	    arg_gpg=${2};
	    shift 2 ;;

	--gpg-home)
	    arg_gpg_home=${2};
	    shift 2 ;;

	*)
	    if [ ! -z "${1}" ]; then
		echo "Unrecognized argument ${1}"
		usage
	    fi
	    break ;;
    esac
done

if [ -z "${arg_arch}" ]; then
    echo "Must specify debian architecture name"
    echo
    usage
fi

if [ -z "${arg_ostree}" ]; then
    echo "Must specify ostree repository directory"
    echo
    usage
fi

if [ -z "${arg_config}" ]; then
    echo "Must specify the multistrap configuration file"
    echo
    usage
fi

if [ -z "${arg_workdir}" ]; then
   cleanup_dir="$(mktemp -d)"
   arg_workdir="${cleanup_dir}"
fi

#####################################################
#                    Do the work                    #
#####################################################
user="$(whoami)"

# Do the debootstrap, remove dev nodes and convert ownership to calling user
echo "Running multistrap"

sudo multistrap -a "${arg_arch}" -d "${arg_workdir}" -f "${arg_config}"
sudo chown -R "${user}:${user}" "${arg_workdir}"

# Before committing, remove the debian apt cache, will save us a half a gig
#rm -rf "${arg_workdir}/var/cache/apt"

# Ensure the ostree repo and make the commit
echo "Committing results to ${arg_ostree} on branch: debian/testing/${arg_arch}"
if [ ! -d "${arg_ostree}" ]; then
    ostree init --mode=archive-z2 --repo="${arg_ostree}"
fi

gpg_args=
if [ ! -z "${arg_gpg}" ]; then
    gpg_args="--gpg-sign=${arg_gpg}"
fi

gpg_home_args=
if [ ! -z "${arg_gpg_home}" ]; then
    gpg_home_args="--gpg-homedir=${arg_gpg_home}"
fi

ostree commit --repo "${arg_ostree}" --branch "debian/testing/${arg_arch}" --skip-if-unchanged ${gpg_args} ${gpg_home_args} -s "Debootstrap commit" "${arg_workdir}"
ostree summary -u --repo "${arg_ostree}" ${gpg_args} ${gpg_home_args}

# Cleanup
if [ ! -z "${cleanup_dir}" ]; then
    echo "Cleaning up temp directory"
    rm -rf "${cleanup_dir}"
else
    echo "Cleaning up work directory at: ${arg_workdir}"
    rm -rf "${arg_workdir}"
fi
echo "Done"
